#
#  Be sure to run `pod spec lint HelloWorld.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#
Pod::Spec.new do |s|
# ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #
s.name          = "framework01"
#s.version      = "INITIAL_VERSION OR TAG"
s.version       = "1.0.0"
s.summary       = "framework01 is a framework"
s.homepage      = "https://gitlab.com/sc4112sc/framework01"
s.description   = "framework01 is a swift framework which has a logger class and printHelloWorld func"
s.license       = { :type => 'MIT', :file => 'LICENSE' }
s.author        = { "sc4112sc" => "sc4112sc@gmail.com" }
s.platform      = :ios, "10.0"
s.ios.vendored_frameworks = 'Framework01.framework'
#s.swift_version = "Swift version of the framework"
s.swift_version = "5.0"
s.source        = { :git => "https://gitlab.com/sc4112sc/framework01.git", :tag => "#{s.version}" }
s.exclude_files = "Classes/Exclude"
s.dependency 'SwiftyJSON'
end